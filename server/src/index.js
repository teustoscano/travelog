require('dotenv').config()
const express = require('express')
const morgan = require('morgan')
const helmet = require('helmet')
const cors = require('cors')
const mongoose = require('mongoose')

const middlewares = require('./middlewares')
const logs = require('./api/logs')

const app = express()

mongoose.connection.on("open", function (ref) {
    console.log("Connected to mongo server.");
});
mongoose.connection.on("error", function (err) {
    console.log("Could not connect to mongo server!");
    return console.log(err);
});

mongoose.connect(process.env.DATABASE_URL, {
    useNewUrlParser: true,
    useUnifiedTopology: true
})
console.log(mongoose.connection.readyState)
app.use(morgan('common'))
app.use(helmet())
app.use(cors({
    origin: process.env.CORS_ORIGIN
}))
// Usado para interpretar o request e poder transmitir a response
app.use(express.json())

app.get('/', (req, res) => {
    res.json({
        message: 'Hello World!'
    })
})

app.use('/api/logs', logs)

// Definindo quando uma rota nao existe
app.use(middlewares.notFound)

// Definindo oq mostrar pra outros tipos de erros que nao sejam o not found
app.use(middlewares.errorHandler)

const port = process.env.PORT || 1337
app.listen(port, () => {
    console.log(`Listening at http://localhost:${port}`)
})