import React, { useState, useEffect } from 'react'
import { useForm } from 'react-hook-form'
import { createLogEntries } from './API'

const LogEntryForm = ({ location, onClose }) => {
    const [loading, setLoading] = useState(false)
    const { register, handleSubmit } = useForm()
    const onSubmit = async (data) => {
        try {
            setLoading(true)
            data.latitude = location.latitude
            data.longitude = location.longitude
            const created = await createLogEntries(data)
            console.log(created)
            onClose()
        } catch (error) {
            console.error(error)
            setLoading(false)
        }
    }
    return (
        <form onSubmit={handleSubmit(onSubmit)} className="entry-form">
            <label htmlFor="title">Title</label>
            <input name="title" required ref={register} />
            <label htmlFor="comments">Comments</label>
            <textarea name="comments" rows={3} ref={register} />
            <label htmlFor="description">Description</label>
            <textarea name="description" rows={3} ref={register} />
            <label htmlFor="image">Image</label>
            <input name="image" ref={register} />
            <label htmlFor="visitDate">Visit Date</label>
            <input name="visitDate" type="date" required ref={register} />
            <button disabled={loading}>{loading ? 'Loading...' : 'Create Entry'}</button>
        </form>
    )
}

export default LogEntryForm